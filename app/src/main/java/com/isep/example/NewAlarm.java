package com.isep.example;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.isep.example.DB.AlarmDBHelper;
import com.isep.example.list.ModelAlarm;

import java.util.ArrayList;
import java.util.Calendar;

public class NewAlarm extends AppCompatActivity {

    private AlarmDBHelper mAlarmDBHelper;

    private static final String TAG="NewAlarm";

    protected CharSequence[] days;

    protected int alarmID = 0;

    protected ModelAlarm mAlarm;

    protected ArrayList<CharSequence> selectedDays = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_alarm);
        EditText name = findViewById(R.id.name);

        mAlarmDBHelper = new AlarmDBHelper(this);
        Bundle b = getIntent().getExtras();

        configureTime();
        setValuesDays();

        FloatingActionButton fab = findViewById(R.id.deleteAlarm);
        FloatingActionButton fabspeech = findViewById(R.id.speech_recorder);
        if(b != null) {
            fabspeech.setVisibility(View.GONE);
            alarmID = (int) b.getInt("alarmId");
            mAlarm = mAlarmDBHelper.getAlarmById(alarmID);
            name.setText(mAlarm.getName());
            setAlarmData();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Interpolator interpolador = AnimationUtils.loadInterpolator(getBaseContext(),
                        android.R.interpolator.fast_out_slow_in);

                fab.animate()
                        .rotation(45f)
                        .setInterpolator(interpolador)
                        .start();
            }
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAlarmDBHelper.delete(mAlarm);
                    Toast toast = Toast.makeText(getApplicationContext(),
                            R.string.alarm_delete_confirm,
                            Toast.LENGTH_SHORT
                    );
                    toast.setGravity(Gravity.BOTTOM | Gravity.LEFT | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast.show();

                    finish();
                }
            });
        } else {
            name.setText(R.string.common_name_new_alarm);
            fab.setVisibility(View.GONE);
            RadioButton rb = findViewById(R.id.onetime);
            rb.setChecked(true);

            fabspeech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(view.getContext(), Recording.class);
                    startActivityForResult(i, 1);
                    //startActivityForResult(i, 1);
                }
            });

        }

        Log.i(getString(R.string.app_name), TAG + " onCreate");
    }

    private void setValuesDays() {
        days = getResources().getStringArray(R.array.day_array);
    }

    protected void configureTime() {
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentMinutes = rightNow.get(Calendar.MINUTE);

        NumberPicker numberPickerHour = findViewById(R.id.hour);
        numberPickerHour.setMaxValue(23);
        numberPickerHour.setMinValue(0);
        numberPickerHour.setValue(currentHour);
        numberPickerHour.setWrapSelectorWheel(true);

        NumberPicker numberPickerMinutes = findViewById(R.id.minutes);
        numberPickerMinutes.setMaxValue(59);
        numberPickerMinutes.setMinValue(0);
        numberPickerMinutes.setValue(currentMinutes);
        numberPickerMinutes.setWrapSelectorWheel(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(getString(R.string.app_name), TAG + " onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(getString(R.string.app_name), TAG + " onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(getString(R.string.app_name), TAG + " onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(getString(R.string.app_name), TAG + " onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(getString(R.string.app_name), TAG + " onDestroy");
    }

    public void createAlarm(View view) {
        RadioGroup radioGroup = findViewById(R.id.days);
        NumberPicker hourElem = findViewById(R.id.hour);
        NumberPicker minutesElem = findViewById(R.id.minutes);
        EditText nameElem = findViewById(R.id.name);
        int hour = hourElem.getValue();
        int minutes = minutesElem.getValue();
        Editable name = nameElem.getText();
        int selectedId = radioGroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        RadioButton radioButton = findViewById(selectedId);
        if(alarmID == 0) {
            mAlarm = new ModelAlarm();
        }

        if(radioButton.getId() == 0) {
            CharSequence daysToSave = getDaysString();
            mAlarm.setDays(daysToSave.toString());
        } else {
            mAlarm.setDays(radioButton.getText().toString());
        }

        mAlarm.setMinutes(minutes);
        mAlarm.setHour(hour);
        mAlarm.setName(name.toString());

        if(alarmID == 0) {
            mAlarmDBHelper.insert(mAlarm);
        } else {
            mAlarmDBHelper.update(mAlarm);
        }

        Toast toast = Toast.makeText(getApplicationContext(),
                R.string.new_alarm_success,
                Toast.LENGTH_SHORT
        );

        toast.setGravity(Gravity.BOTTOM | Gravity.LEFT | Gravity.FILL_HORIZONTAL, 0, 0);
        toast.show();

        finish();
    }

    /*private StringBuilder getDaysByDefaultOptions(RadioButton option) {
        if(option.getId() == 0) {
            return getDaysString();
        }

        selectedDays.clear();
        return (StringBuilder) option.getText();
        if (optionValue == getResources().getText(R.string.option_all_days)) {
            int count = days.length;
            for(int i = 0; i < count; i++) {
                selectedDays.add(days[i]);
            }
        } else if (optionValue == getResources().getText(R.string.option_on_the_week)) {
            selectedDays.add(getResources().getText(R.string.day_monday));
            selectedDays.add(getResources().getText(R.string.day_thursday));
            selectedDays.add(getResources().getText(R.string.day_wednesday));
            selectedDays.add(getResources().getText(R.string.day_thursday));
            selectedDays.add(getResources().getText(R.string.day_friday));
        } else if (optionValue == getResources().getText(R.string.option_one_time)) {
            selectedDays.add(getResources().getText(R.string.day_monday));
        } else if (optionValue == getResources().getText(R.string.option_only_weekend)) {
            selectedDays.add(getResources().getText(R.string.day_saturday));
            selectedDays.add(getResources().getText(R.string.day_sunday));
        }
        return optionValue;
    }*/

    private void setAlarmData() {
        NumberPicker numberPickerHour = findViewById(R.id.hour);
        numberPickerHour.setValue(mAlarm.getHour());

        NumberPicker numberPickerMinutes = findViewById(R.id.minutes);
        numberPickerMinutes.setValue(mAlarm.getMinutes());

        if(mAlarm.getDays().equals(getResources().getText(R.string.option_all_days).toString())) {
            RadioButton rb = findViewById(R.id.alldays);
            rb.setChecked(true);
        } else if (mAlarm.getDays().equals(getResources().getText(R.string.option_on_the_week).toString())) {
            RadioButton rb = findViewById(R.id.ontheweek);
            rb.setChecked(true);
        } else if (mAlarm.getDays().equals(getResources().getText(R.string.option_only_weekend).toString())) {
            RadioButton rb = findViewById(R.id.weekends);
            rb.setChecked(true);
        } else if (mAlarm.getDays().equals(getResources().getText(R.string.option_one_time).toString())) {
            RadioButton rb = findViewById(R.id.onetime);
            rb.setChecked(true);
        } else {
            for (String s: mAlarm.getDaysArray()) {
                selectedDays.add(s);
            }
            applyDays();
        }
    }

    public void selectDays(View view) {
        // If the user can configurate the alarm
        int count = days.length;
        boolean[] checkedDays = new boolean[count];

        for(int i = 0; i < count; i++) {
            checkedDays[i] = selectedDays.contains(days[i]);
        }

        DialogInterface.OnMultiChoiceClickListener daysDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked) {
                    selectedDays.add(days[which]);
                }
                else {
                    selectedDays.remove(days[which]);
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
            .setTitle(R.string.alert_select_days_name)
            .setMultiChoiceItems(days, checkedDays, daysDialogListener)
            .setCancelable(false)
            .setPositiveButton(R.string.ok_select_day, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    applyDays();
                    dialog.dismiss();
                }
            });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void applyDays() {
        StringBuilder stringBuilder = getDaysString();
        RadioGroup group = findViewById(R.id.days);
        RadioButton newButton = new RadioButton(this);
        newButton.setText(stringBuilder);
        newButton.setChecked(true);
        newButton.setId(0);
        group.addView(newButton);
    }

    private StringBuilder getDaysString() {
        StringBuilder stringBuilder = new StringBuilder();

        int count = 0;
        for(CharSequence days : selectedDays) {
            if((selectedDays.size() - 1) == count) {
                stringBuilder.append(days);
            } else {
                stringBuilder.append(days + ", ");
            }
            count++;
        }

        return stringBuilder;
    }
}
