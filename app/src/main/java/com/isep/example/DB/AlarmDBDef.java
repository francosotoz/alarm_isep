package com.isep.example.DB;

public class AlarmDBDef {
    public static final String DATABASE_NAME = "ISEPDBAlarm";
    public static final int DATABASE_VERSION = 1;

    public static class ALARMS {
        public static final String TABLE_NAME = "alarms";
        public static final String ID_COL = "id";
        public static final String NAME_COL = "name";
        public static final String HOUR_COL = "hour";
        public static final String MINUTES_COL = "minutes";
        public static final String DAYS_COL = "days";
        public static final String ACTIVATE_COL = "activate";
    }

    //Setencia SQL que permite crear la tabla Notes
    public static final String ALARMS_TABLE_CREATE =
            "CREATE TABLE " + ALARMS.TABLE_NAME + " (" +
                    ALARMS.ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ALARMS.NAME_COL + " TEXT, " +
                    ALARMS.HOUR_COL + " INTEGER, " +
                    ALARMS.MINUTES_COL + " INTEGER, " +
                    ALARMS.DAYS_COL + " TEXT, " +
                    ALARMS.ACTIVATE_COL + " BOOLEAN);";

    //Setencia SQL que permite eliminar la tabla Notes
    public static final String ALARMS_TABLE_DROP = "DROP TABLE IF EXISTS " + ALARMS.TABLE_NAME;
}
