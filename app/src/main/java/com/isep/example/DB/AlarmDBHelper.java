package com.isep.example.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.isep.example.list.ModelAlarm;

import java.util.ArrayList;

public class AlarmDBHelper extends SQLiteOpenHelper {


    public AlarmDBHelper(Context context) {
        super(context, AlarmDBDef.DATABASE_NAME, null, AlarmDBDef.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Creamos las tablas en la Base de datos
        db.execSQL(AlarmDBDef.ALARMS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(AlarmDBDef.ALARMS_TABLE_DROP);
        this.onCreate(db);
    }


    /*
     * OPERACIONES CRUD (Create, Read, Update, Delete)
     * */

    public void insert(ModelAlarm alarm){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AlarmDBDef.ALARMS.HOUR_COL, alarm.getHour());
        values.put(AlarmDBDef.ALARMS.MINUTES_COL, alarm.getMinutes());
        values.put(AlarmDBDef.ALARMS.NAME_COL, alarm.getName());
        values.put(AlarmDBDef.ALARMS.ACTIVATE_COL, alarm.getActivate());
        values.put(AlarmDBDef.ALARMS.DAYS_COL, alarm.getDays());

        db.insert(AlarmDBDef.ALARMS.TABLE_NAME, null, values);

        db.close();
    }

    public ModelAlarm getAlarmById(int id){
        ModelAlarm aAlarm = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String[] COLUMNS = {
                AlarmDBDef.ALARMS.ID_COL,
                AlarmDBDef.ALARMS.NAME_COL,
                AlarmDBDef.ALARMS.HOUR_COL,
                AlarmDBDef.ALARMS.MINUTES_COL,
                AlarmDBDef.ALARMS.DAYS_COL,
                AlarmDBDef.ALARMS.ACTIVATE_COL
        };

        Cursor cursor = db.query(AlarmDBDef.ALARMS.TABLE_NAME,
                COLUMNS,
                " id = ?",
                new String[] { String.valueOf(id) },
                null,
                null,
                null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();
            aAlarm = genAlarm(cursor);
        }

        return aAlarm;
    }


    public ArrayList<ModelAlarm> getAll() {
        ArrayList notes = new ArrayList();

        String query = "SELECT  * FROM " + AlarmDBDef.ALARMS.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        ModelAlarm aAlarm;
        if (cursor.moveToFirst()) {
            do {
                aAlarm = genAlarm(cursor);
                notes.add(aAlarm);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return notes;
    }

    public int update(ModelAlarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AlarmDBDef.ALARMS.NAME_COL, alarm.getName());
        values.put(AlarmDBDef.ALARMS.HOUR_COL, alarm.getHour());
        values.put(AlarmDBDef.ALARMS.MINUTES_COL, alarm.getMinutes());
        values.put(AlarmDBDef.ALARMS.ACTIVATE_COL, alarm.getActivate());
        values.put(AlarmDBDef.ALARMS.DAYS_COL, alarm.getDays());

        int i = db.update(AlarmDBDef.ALARMS.TABLE_NAME,
                values,
                AlarmDBDef.ALARMS.ID_COL+" = ?",
                new String[] { String.valueOf(alarm.getId()) });

        db.close();

        return i;
    }

    public void delete(ModelAlarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(AlarmDBDef.ALARMS.TABLE_NAME,
                AlarmDBDef.ALARMS.ID_COL+" = ?",
                new String[] { String.valueOf(alarm.getId()) });

        db.close();
    }

    private ModelAlarm genAlarm(Cursor cursor) {
        ModelAlarm aAlarm = new ModelAlarm();
        aAlarm.setId(Integer.parseInt(cursor.getString(0)));
        aAlarm.setName(cursor.getString(1));
        aAlarm.setHour(cursor.getInt(2));
        aAlarm.setMinutes(cursor.getInt(3));
        aAlarm.setDays(cursor.getString(4));
        aAlarm.setActivate(cursor.getInt(5) == 1);

        return aAlarm;
    }

}
