package com.isep.example;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.isep.example.Adapter.AdapterAlarm;
import com.isep.example.DB.AlarmDBHelper;
import com.isep.example.list.ModelAlarm;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private AlarmDBHelper mAlarmDBHelper;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recicler_view);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAlarmDBHelper = new AlarmDBHelper(this);

        loadAlarmsList();

        FloatingActionButton fab = findViewById(R.id.new_alarm);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), NewAlarm.class);
                startActivityForResult(i, 1);
            }
        });

        Log.i(getString(R.string.app_name), TAG + " onCreate");
    }

    private void loadAlarmsList() {
        ArrayList<ModelAlarm> list = mAlarmDBHelper.getAll();

        mAdapter = new AdapterAlarm(list, new AdapterAlarm.OnItemClickListener() {
            public void onItemClick(ModelAlarm item) {
                Intent i = new Intent(MainActivity.this, NewAlarm.class);
                i.putExtra("alarmId", item.getId());
                startActivityForResult(i, 1);
            }
        }, mAlarmDBHelper, this);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(getString(R.string.app_name), TAG + " onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(getString(R.string.app_name), TAG + " onStop");
    }

    @Override
    protected void onResume() {
        loadAlarmsList();
        super.onResume();
        Log.i(getString(R.string.app_name), TAG + " onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(getString(R.string.app_name), TAG + " onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(getString(R.string.app_name), TAG + " onDestroy");
    }
}
