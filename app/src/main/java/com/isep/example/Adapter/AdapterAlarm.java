package com.isep.example.Adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.isep.example.DB.AlarmDBHelper;
import com.isep.example.R;
import com.isep.example.list.ModelAlarm;

import java.util.ArrayList;

public class AdapterAlarm extends RecyclerView.Adapter<AdapterAlarm.AlarmViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(ModelAlarm item);
    }

    private ArrayList<ModelAlarm> alarms;
    private AdapterAlarm.OnItemClickListener mListener;
    private AlarmDBHelper mAlarmDBHelper;
    private AppCompatActivity contextParent;

    public static class AlarmViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextHour;
        public TextView mTextDays;
        public Switch mActivate;

        public AlarmViewHolder(View v) {
            super(v);
            mTextHour = (TextView) itemView.findViewById(R.id.hour);
            mTextDays = (TextView) itemView.findViewById(R.id.days);
            mActivate = (Switch) itemView.findViewById(R.id.active);
        }

        public void bind(final ModelAlarm item, final AdapterAlarm.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public AdapterAlarm(ArrayList<ModelAlarm> data,
                        AdapterAlarm.OnItemClickListener listener,
                        AlarmDBHelper alarmDBHelper,
                        AppCompatActivity context) {
        alarms = data;
        mListener = listener;
        mAlarmDBHelper = alarmDBHelper;
        contextParent = context;
    }

    @Override
    public AdapterAlarm.AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_alarm, parent, false);

        AlarmViewHolder vh = new AlarmViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AlarmViewHolder holder, int position) {
        final ModelAlarm model = alarms.get(position);
        holder.mTextHour.setText(model.getHourAndMinutes());
        holder.mTextDays.setText(model.getDays());
        holder.mActivate.setChecked(model.getActivate());
        Log.i("AdapterAlarm", model.getDays() + " - " + position);
        holder.bind(model, mListener);

        holder.mActivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                model.setActivate(isChecked);
                mAlarmDBHelper.update(model);
                Toast toast = Toast.makeText(contextParent,
                        isChecked ? R.string.alarm_enabled : R.string.alarm_disabled,
                    Toast.LENGTH_SHORT
                );
                toast.setGravity(Gravity.BOTTOM | Gravity.LEFT | Gravity.FILL_HORIZONTAL, 0, 0);
                toast.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void add(ModelAlarm item) {
        alarms.add(item);
        notifyItemInserted(alarms.size() - 1);
    }
}

