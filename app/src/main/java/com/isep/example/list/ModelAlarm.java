package com.isep.example.list;

import android.content.Context;

public class ModelAlarm {
    private int mId;
    private int mHour;
    private int mMinutes;
    private String mName;
    private String mDays;
    private Boolean mActivate;
    private Context context;

    public ModelAlarm() {
        mActivate = true;
    }

    public int getId() {
        return mId;
    }

    public int getHour() {
        return mHour;
    }

    public int getMinutes() {
        return mMinutes;
    }

    public String getName() {
        return mName;
    }

    public Boolean getActivate() {
        return mActivate;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setHour(int hour) {
        mHour = hour;
    }

    public void setActivate(Boolean activate) {
        mActivate = activate;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setDays(String days) {
        mDays = days;
    }

    public void setMinutes(int min) {
        mMinutes = min;
    }

    public String getHourAndMinutes() {
        String hour = (mHour < 10) ? "0" + mHour : "" + mHour;
        String aMinutes = (mMinutes < 10) ? "0" + mMinutes : "" + mMinutes;
        return hour + ":" + aMinutes;
    }

    public String getDays() {
        return mDays;
    }

    public String[] getDaysArray() {
        String[] split = mDays.split(",");
        return split;
    }

    public String[] setContext() {
        String[] split = mDays.split(",");
        return split;
    }
/*
    public CharSequence getOptionValue() {
        String[] days = getDaysArray();
        if (optionValue == getResources().getText(R.string.option_all_days)) {
            int count = days.length;
            for(int i = 0; i < count; i++) {
                selectedDays.add(days[i]);
            }
        } else if (optionValue == getResources().getText(R.string.option_on_the_week)) {
            selectedDays.add(getResources().getText(R.string.day_monday));
            selectedDays.add(getResources().getText(R.string.day_thursday));
            selectedDays.add(getResources().getText(R.string.day_wednesday));
            selectedDays.add(getResources().getText(R.string.day_thursday));
            selectedDays.add(getResources().getText(R.string.day_friday));
        } else if (optionValue == getResources().getText(R.string.option_one_time)) {
            selectedDays.add(getResources().getText(R.string.day_monday));
        } else if (optionValue == getResources().getText(R.string.option_only_weekend)) {
            selectedDays.add(getResources().getText(R.string.day_saturday));
            selectedDays.add(getResources().getText(R.string.day_sunday));
        }
    }*/
}
